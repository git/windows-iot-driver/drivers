/*++
Copyright (c) Texas Instruments Inc. All Rights Reserved.
Sample code for Dual LED FLASH LM3643.

The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Module Name:

    device.c - Device handling events for example driver.

Abstract:

   This file contains the device entry points and callbacks.
    
Environment:

    Kernel-mode Driver Framework

--*/

#include "driver.h"
#include "device.tmh"
#include "spb.h"
#include "public.h"

#ifdef ALLOC_PRAGMA
	#pragma alloc_text(PAGE, OnD0Exit)
#endif


NTSTATUS
RegUpdateSynchronously(
	IN SPB_CONTEXT *SpbContext,
	IN UCHAR Address,
	IN UCHAR Mask,
	IN UCHAR Data
	)
{
	NTSTATUS status = STATUS_SUCCESS;
	BYTE rData;

	PAGED_CODE();

	status = SpbReadDataSynchronously(SpbContext, Address, &rData, 1);
	if (!NT_SUCCESS(status))
		return status;

	rData &= ~Mask;
	rData |= (Data & Mask);

	status = SpbWriteDataSynchronously(SpbContext, Address, &rData, 1);

	return status;
}

NTSTATUS
OnD0Entry(
	IN WDFDEVICE Device,
	IN WDF_POWER_DEVICE_STATE PreviousState
	)
	/*++

	Routine Description:

	This routine will power on the hardware

	Arguments:

	Device - WDF device to power on
	PreviousState - Prior power state

	Return Value:

	NTSTATUS indicating success or failure

	*/
{
	NTSTATUS status = STATUS_SUCCESS;
	PDEVICE_EXTENSION devContext;

	devContext = GetDeviceContext(Device);

	UNREFERENCED_PARAMETER(PreviousState);

	devContext->ServiceInterruptsAfterD0Entry = TRUE;

	return status;
}

NTSTATUS
OnD0Exit(
	IN WDFDEVICE Device,
	IN WDF_POWER_DEVICE_STATE TargetState
	)
	/*++

	Routine Description:

	This routine will power down the hardware

	Arguments:

	Device - WDF device to power off

	PreviousState - Prior power state

	Return Value:

	NTSTATUS indicating success or failure

	*/
{
	NTSTATUS status = STATUS_SUCCESS;
	PDEVICE_EXTENSION devContext;

	UNREFERENCED_PARAMETER(TargetState);

	PAGED_CODE();

	devContext = GetDeviceContext(Device);

	SPB_CONTEXT *SpbContext = &devContext->I2CContext;
	status = RegUpdateSynchronously(SpbContext, 0x01, 0x03, 0);
	return status;
}

NTSTATUS
OnPrepareHardware(
	IN WDFDEVICE FxDevice,
	IN WDFCMRESLIST FxResourcesRaw,
	IN WDFCMRESLIST FxResourcesTranslated
	)
	/*++

	Routine Description:

	This routine is called by the PnP manager and supplies thie device instance
	with it's SPB resources (CmResourceTypeConnection) needed to find the I2C
	driver.

	Arguments:

	FxDevice - a handle to the framework device object
	FxResourcesRaw - list of translated hardware resources that
	the PnP manager has assigned to the device
	FxResourcesTranslated - list of raw hardware resources that
	the PnP manager has assigned to the device

	Return Value:

	NTSTATUS indicating sucess or failure

	--*/
{
	NTSTATUS status;
	PCM_PARTIAL_RESOURCE_DESCRIPTOR res;
	PDEVICE_EXTENSION devContext;
	ULONG resourceCount;
	ULONG i;

	UNREFERENCED_PARAMETER(FxResourcesRaw);

	PAGED_CODE();

	status = STATUS_INSUFFICIENT_RESOURCES;
	devContext = GetDeviceContext(FxDevice);
	resourceCount = WdfCmResourceListGetCount(FxResourcesTranslated);

	for (i = 0; i < resourceCount; i++)
	{
		res = WdfCmResourceListGetDescriptor(FxResourcesTranslated, i);

		if (res->Type == CmResourceTypeConnection &&
			res->u.Connection.Class == CM_RESOURCE_CONNECTION_CLASS_SERIAL &&
			res->u.Connection.Type == CM_RESOURCE_CONNECTION_TYPE_SERIAL_I2C)
		{
			devContext->I2CContext.I2cResHubId.LowPart =
				res->u.Connection.IdLowPart;
			devContext->I2CContext.I2cResHubId.HighPart =
				res->u.Connection.IdHighPart;

			status = STATUS_SUCCESS;
		}
	}

	if (!NT_SUCCESS(status))
		goto exit;

	//
	// Initialize Spb so the driver can issue reads/writes
	//
	status = SpbTargetInitialize(FxDevice, &devContext->I2CContext);

	if (!NT_SUCCESS(status))
		goto exit;
	
	// 
	// Read Device Id - LM3643 ID should be 0x02
	// 
	BYTE rData;
	SPB_CONTEXT *SpbContext = &devContext->I2CContext;
	status = SpbReadDataSynchronously(SpbContext, 0x0C, &rData, 1);

	// 
	// Add additional chip initialize code here if it is needed
	// such as TX-pin, IVFM Levels/Hysteresis, Selection etc 
	
exit:

	return status;
}

NTSTATUS
OnReleaseHardware(
	IN WDFDEVICE FxDevice,
	IN WDFCMRESLIST FxResourcesTranslated
	)
	/*++

	Routine Description:

	This routine cleans up any resources provided.

	Arguments:

	FxDevice - a handle to the framework device object
	FxResourcesRaw - list of translated hardware resources that
	the PnP manager has assigned to the device
	FxResourcesTranslated - list of raw hardware resources that
	the PnP manager has assigned to the device

	Return Value:

	NTSTATUS indicating sucesss or failure

	--*/
{
	NTSTATUS status;
	PDEVICE_EXTENSION devContext;
	SPB_CONTEXT *SpbContext;
	UNREFERENCED_PARAMETER(FxResourcesTranslated);

	PAGED_CODE();

	devContext = GetDeviceContext(FxDevice);
	
	BYTE rData;
	SPB_CONTEXT *SpbContext = &devContext->I2CContext;
	status = SpbReadDataSynchronously(SpbContext, 0x01, &rData, 1);
	if (!NT_SUCCESS(status))
	{
		return status;
	}

	rData &= (~0x03); //LED1, 2 OFF
	SpbWriteDataSynchronously(SpbContext, 0x01, &rData, 1);
	if (!NT_SUCCESS(status))
	{
		return status;
	}

	SpbTargetDeinitialize(FxDevice, &GetDeviceContext(FxDevice)->I2CContext);

	return status;
}



VOID
OnDeviceControl(
	IN WDFQUEUE Queue,
	IN WDFREQUEST Request,
	IN size_t OutputBufferLength,
	IN size_t InputBufferLength,
	IN ULONG IoControlCode
	)
	/*++

	Routine Description:

	This event is called when the framework receives
	IRP_MJ_INTERNAL DEVICE_CONTROL requests from the system.

	Arguments:

	Queue - Handle to the framework queue object that is associated
	with the I/O request.

	Request - Handle to a framework request object.

	OutputBufferLength - length of the request's output buffer,
	if an output buffer is available.

	InputBufferLength - length of the request's input buffer,
	if an input buffer is available.

	IoControlCode - the driver-defined or system-defined I/O control code
	(IOCTL) that is associated with the request.

	Return Value:

	None, status is indicated when completing the request

	--*/
{
	NTSTATUS status = STATUS_SUCCESS;
	PDEVICE_EXTENSION devContext;
	WDFDEVICE device;
	BOOLEAN requestPending;
	BYTE rData = 0, wData = 0;
	USHORT rData2b = 0;
	ULONG* inputVal;
	ULONG* retVal;
	SPB_CONTEXT *SpbContext;
	UNREFERENCED_PARAMETER(OutputBufferLength);
	UNREFERENCED_PARAMETER(InputBufferLength);

	PAGED_CODE();

	device = WdfIoQueueGetDevice(Queue);
	devContext = GetDeviceContext(device);
	requestPending = FALSE;

	SpbContext = &devContext->I2CContext;
	
	WdfRequestRetrieveInputBuffer(Request, sizeof(ULONG), (void**)&inputVal, NULL);
	WdfRequestRetrieveOutputBuffer(Request, sizeof(ULONG),(void**)&retVal, NULL);
	wData = (*inputVal) & 0xff;

	switch (IoControlCode) {

	case IOCTL_LM3643_DEVID:
		status = SpbReadDataSynchronously(SpbContext, 0x0C, &rData, 1);
		*retVal = (ULONG)rData;
		break;

	case  IOCTL_LM3643_MODE:
		status = RegUpdateSynchronously(SpbContext, 0x01, 0x0C, (wData << 2));
		status |= SpbReadDataSynchronously(SpbContext, 0x01, &rData, 1);
		*retVal = (ULONG)rData;
		break;

	case  IOCTL_LM3643_TORCH_PIN_ENABLE:
		status = RegUpdateSynchronously(SpbContext, 0x01, 0x10, (wData << 4));
		status |= SpbReadDataSynchronously(SpbContext, 0x01, &rData, 1);
		*retVal = (ULONG)rData;
		break;

	case  IOCTL_LM3643_STROBE_PIN_ENABLE:
		status = RegUpdateSynchronously(SpbContext, 0x01, 0x20, (wData << 5));
		status |= SpbReadDataSynchronously(SpbContext, 0x01, &rData, 1);
		*retVal = (ULONG)rData;
		break;

	case  IOCTL_LM3643_STATUS:		
		status = SpbReadDataSynchronously(SpbContext, 0x0A, &rData, 1);
		rData2b |= ( ( rData& 0xff) << 8);
		status = SpbReadDataSynchronously(SpbContext, 0x0B, &rData, 1);
		rData2b |= (rData & 0xff);
		*retVal = (ULONG)rData2b;
		break;

	case  IOCTL_LM3643_LED1_ENABLE:
		status = RegUpdateSynchronously(SpbContext, 0x01, 0x01, (wData << 0));
		status |= SpbReadDataSynchronously(SpbContext, 0x01, &rData, 1);
		*retVal = (ULONG)rData;
		break;

	case  IOCTL_LM3643_LED1_BRT_FLASH:
		status = RegUpdateSynchronously(SpbContext, 0x03, 0x3f, (wData << 0));
		status |= SpbReadDataSynchronously(SpbContext, 0x03, &rData, 1);
		*retVal = (ULONG)rData;
		break;

	case  IOCTL_LM3643_LED1_BRT_TORCH:
		status = RegUpdateSynchronously(SpbContext, 0x05, 0x3f, (wData << 0));
		status |= SpbReadDataSynchronously(SpbContext, 0x05, &rData, 1);
		*retVal = (ULONG)rData;
		break;

	case  IOCTL_LM3643_LED2_ENABLE:
		status = RegUpdateSynchronously(SpbContext, 0x01, 0x02, (wData << 1));
		status |= SpbReadDataSynchronously(SpbContext, 0x01, &rData, 1);
		*retVal = (ULONG)rData;
		break;

	case  IOCTL_LM3643_LED2_BRT_FLASH:
		status = RegUpdateSynchronously(SpbContext, 0x04, 0x3f, (wData << 0));
		status |= SpbReadDataSynchronously(SpbContext, 0x04, &rData, 1);
		*retVal = (ULONG)rData;
		break;

	case  IOCTL_LM3643_LED2_BRT_TORCH:
		status = RegUpdateSynchronously(SpbContext, 0x06, 0x3f, (wData << 0));
		status |= SpbReadDataSynchronously(SpbContext, 0x06, &rData, 1);
		*retVal = (ULONG)rData;
		break;

	case  IOCTL_LM3643_FLASH_TOUT:
		status = RegUpdateSynchronously(SpbContext, 0x08, 0x0f, (wData << 0));
		status |= SpbReadDataSynchronously(SpbContext, 0x08, &rData, 1);
		*retVal = (ULONG)rData;
		break;

	default:
		status = STATUS_NOT_SUPPORTED;
		break;
	}

	if (!requestPending)
	{
		WdfRequestCompleteWithInformation(Request, status, sizeof(ULONG));
	}

	return;
}
