/*++
Copyright (c) Texas Instruments Inc. All Rights Reserved.
Sample code for LED FLASH LM3642.

The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Module Name:

device.c - Device handling events for example driver.

Abstract:

This file contains the device entry points and callbacks.

Environment:

Kernel-mode Driver Framework

--*/

#include "driver.h"
#include "device.tmh"
#include "spb.h"
#include "public.h"

#ifdef ALLOC_PRAGMA
#pragma alloc_text(PAGE, OnD0Exit)
#endif

// LM3642 Registers
#define REG_REVISION		0x00
#define REG_REVISION_MASK	0x07

//IVFM Mode Register
#define REG_IVFM_MODE		0x01
#define REG_IVFM_MODE_THRESHOLD_MASK	0x1C

// Torch Ramp Time Reister
#define REG_TORCH_RAMP		0x06
#define REG_TORCH_RAMP_UP_TIME_MASK		0x38
#define REG_TORCH_RAMP_DN_TIME_MASK		0x07

//Flash Freature Register
#define REG_FLASH_FEATURE					0x08
#define REG_FLASH_FEATURE_RAMPTIME_MASK		0x38
#define REG_FLASH_FEATURE_RAMPTIME_SHIFT	3
#define REG_FLASH_FEATURE_TIMEOUT_MASK		0x07
#define REG_FLASH_FEATURE_TIMEOUT_SHIFT		0

//Current Control Register
#define REG_CURRENT_CTRL				0x09
#define REG_CURRENT_CTRL_FLASH_MASK		0x0F
#define REG_CURRENT_CTRL_FLASH_SHIFT	0
#define REG_CURRENT_CTRL_TORCH_MASK		0x70
#define REG_CURRENT_CTRL_TORCH_SHIFT	4

//Enable Register
#define REG_ENABLE				0x0A
#define REG_ENABLE_MODE_MASK	0x03
#define REG_ENABLE_MODE_SHIFT	0

//Flag Registers
#define REG_FLAG		0x0B
#define REG_FLAG_MASK	0x1F


#define FLED_REG_MODE_ADDR REG_ENABLE
#define FLED_REG_MODE_MASK REG_ENABLE_MODE_MASK
#define FLED_REG_MODE_OFF  0

NTSTATUS
RegUpdateSynchronously(
	IN SPB_CONTEXT *SpbContext,
	IN UCHAR Address,
	IN UCHAR Mask,
	IN UCHAR Data
	)
{
	NTSTATUS status = STATUS_SUCCESS;
	BYTE rData;

	PAGED_CODE();

	status = SpbReadDataSynchronously(SpbContext, Address, &rData, 1);
	if (!NT_SUCCESS(status))
		return status;

	rData &= ~Mask;
	rData |= (Data & Mask);

	status = SpbWriteDataSynchronously(SpbContext, Address, &rData, 1);

	return status;
}

NTSTATUS
OnD0Entry(
	IN WDFDEVICE Device,
	IN WDF_POWER_DEVICE_STATE PreviousState
	)
	/*++

	Routine Description:

	This routine will power on the hardware

	Arguments:

	Device - WDF device to power on
	PreviousState - Prior power state

	Return Value:

	NTSTATUS indicating success or failure

	*/
{
	NTSTATUS status = STATUS_SUCCESS;
	PDEVICE_EXTENSION devContext;

	devContext = GetDeviceContext(Device);

	UNREFERENCED_PARAMETER(PreviousState);

	devContext->ServiceInterruptsAfterD0Entry = TRUE;

	return status;
}

NTSTATUS
OnD0Exit(
	IN WDFDEVICE Device,
	IN WDF_POWER_DEVICE_STATE TargetState
	)
	/*++

	Routine Description:

	This routine will power down the hardware

	Arguments:

	Device - WDF device to power off

	PreviousState - Prior power state

	Return Value:

	NTSTATUS indicating success or failure

	*/
{
	NTSTATUS status = STATUS_SUCCESS;
	PDEVICE_EXTENSION devContext;

	UNREFERENCED_PARAMETER(TargetState);

	PAGED_CODE();

	devContext = GetDeviceContext(Device);

	SPB_CONTEXT *SpbContext = &devContext->I2CContext;
	status = RegUpdateSynchronously(SpbContext, FLED_REG_MODE_ADDR, 
							        FLED_REG_MODE_MASK, FLED_REG_MODE_OFF);
	return status;
}

NTSTATUS
OnPrepareHardware(
	IN WDFDEVICE FxDevice,
	IN WDFCMRESLIST FxResourcesRaw,
	IN WDFCMRESLIST FxResourcesTranslated
	)
	/*++

	Routine Description:

	This routine is called by the PnP manager and supplies thie device instance
	with it's SPB resources (CmResourceTypeConnection) needed to find the I2C
	driver.

	Arguments:

	FxDevice - a handle to the framework device object
	FxResourcesRaw - list of translated hardware resources that
	the PnP manager has assigned to the device
	FxResourcesTranslated - list of raw hardware resources that
	the PnP manager has assigned to the device

	Return Value:

	NTSTATUS indicating sucess or failure

	--*/
{
	NTSTATUS status;
	PCM_PARTIAL_RESOURCE_DESCRIPTOR res;
	PDEVICE_EXTENSION devContext;
	ULONG resourceCount;
	ULONG i;

	UNREFERENCED_PARAMETER(FxResourcesRaw);

	PAGED_CODE();

	status = STATUS_INSUFFICIENT_RESOURCES;
	devContext = GetDeviceContext(FxDevice);
	resourceCount = WdfCmResourceListGetCount(FxResourcesTranslated);

	for (i = 0; i < resourceCount; i++)
	{
		res = WdfCmResourceListGetDescriptor(FxResourcesTranslated, i);

		if (res->Type == CmResourceTypeConnection &&
			res->u.Connection.Class == CM_RESOURCE_CONNECTION_CLASS_SERIAL &&
			res->u.Connection.Type == CM_RESOURCE_CONNECTION_TYPE_SERIAL_I2C)
		{
			devContext->I2CContext.I2cResHubId.LowPart =
				res->u.Connection.IdLowPart;
			devContext->I2CContext.I2cResHubId.HighPart =
				res->u.Connection.IdHighPart;

			status = STATUS_SUCCESS;
		}
	}

	if (!NT_SUCCESS(status))
		goto exit;

	//
	// Initialize Spb so the driver can issue reads/writes
	//
	status = SpbTargetInitialize(FxDevice, &devContext->I2CContext);

	if (!NT_SUCCESS(status))
		goto exit;

	// 
	// Read Device Id - LM3642 ID should be 000
	// 
	BYTE rData;
	SPB_CONTEXT *SpbContext = &devContext->I2CContext;
	status = SpbReadDataSynchronously(SpbContext, REG_REVISION, &rData, 1);
	if (!NT_SUCCESS(status))
		goto exit;

	// StandBy MODE Set or Disable
	status = RegUpdateSynchronously(SpbContext, FLED_REG_MODE_ADDR,
		FLED_REG_MODE_MASK, FLED_REG_MODE_OFF);
	// 
	// Add additional chip initialize code here if it is needed
	// such as TX-pin, IVFM Levels/Hysteresis, Selection etc 

exit:

	return status;
}

NTSTATUS
OnReleaseHardware(
	IN WDFDEVICE FxDevice,
	IN WDFCMRESLIST FxResourcesTranslated
	)
	/*++

	Routine Description:

	This routine cleans up any resources provided.

	Arguments:

	FxDevice - a handle to the framework device object
	FxResourcesRaw - list of translated hardware resources that
	the PnP manager has assigned to the device
	FxResourcesTranslated - list of raw hardware resources that
	the PnP manager has assigned to the device

	Return Value:

	NTSTATUS indicating sucesss or failure

	--*/
{
	NTSTATUS status;
	PDEVICE_EXTENSION devContext;
	SPB_CONTEXT *SpbContext;
	UNREFERENCED_PARAMETER(FxResourcesTranslated);

	PAGED_CODE();

	devContext = GetDeviceContext(FxDevice);

	//Standby Mode or Disable
	SpbContext = &devContext->I2CContext;
	status = RegUpdateSynchronously(SpbContext, FLED_REG_MODE_ADDR,
									FLED_REG_MODE_MASK, FLED_REG_MODE_OFF);
	if (!NT_SUCCESS(status))
	{
		return status;
	}

	SpbTargetDeinitialize(FxDevice, &GetDeviceContext(FxDevice)->I2CContext);

	return status;
}



VOID
OnDeviceControl(
	IN WDFQUEUE Queue,
	IN WDFREQUEST Request,
	IN size_t OutputBufferLength,
	IN size_t InputBufferLength,
	IN ULONG IoControlCode
	)
	/*++

	Routine Description:

	This event is called when the framework receives
	IRP_MJ_INTERNAL DEVICE_CONTROL requests from the system.

	Arguments:

	Queue - Handle to the framework queue object that is associated
	with the I/O request.

	Request - Handle to a framework request object.

	OutputBufferLength - length of the request's output buffer,
	if an output buffer is available.

	InputBufferLength - length of the request's input buffer,
	if an input buffer is available.

	IoControlCode - the driver-defined or system-defined I/O control code
	(IOCTL) that is associated with the request.

	Return Value:

	None, status is indicated when completing the request

	--*/
{
	NTSTATUS status = STATUS_SUCCESS;
	PDEVICE_EXTENSION devContext;
	WDFDEVICE device;
	BOOLEAN requestPending;
	BYTE rData = 0, wData = 0;
	ULONG* inputVal;
	ULONG* retVal;
	SPB_CONTEXT *SpbContext;
	UNREFERENCED_PARAMETER(OutputBufferLength);
	UNREFERENCED_PARAMETER(InputBufferLength);

	PAGED_CODE();

	device = WdfIoQueueGetDevice(Queue);
	devContext = GetDeviceContext(device);
	requestPending = FALSE;

	SpbContext = &devContext->I2CContext;

	WdfRequestRetrieveInputBuffer(Request, sizeof(ULONG), (void**)&inputVal, NULL);
	WdfRequestRetrieveOutputBuffer(Request, sizeof(ULONG), (void**)&retVal, NULL);
	wData = (*inputVal) & 0xff;
	
	switch (IoControlCode) {

	case IOCTL_FLED_DEVID:
		status = SpbReadDataSynchronously(SpbContext, REG_REVISION, &rData, 1);
		if (!NT_SUCCESS(status))
			goto end;
		*retVal = (ULONG)(rData & REG_REVISION_MASK);
		break;

	case  IOCTL_FLED_MODE:
		status = SpbUpdateSynchronously(SpbContext,
			REG_ENABLE, REG_ENABLE_MODE_MASK,
			wData << REG_ENABLE_MODE_SHIFT);
		if (!NT_SUCCESS(status))
			goto end;
		break;

	case  IOCTL_FLED_STATUS:
		status = SpbReadDataSynchronously(SpbContext, REG_FLAG, &rData, 1);
		if (!NT_SUCCESS(status))
			goto end;
		*retVal = (ULONG)(rData & REG_FLAG_MASK);
		break;

	case  IOCTL_FLED_BRT_FLASH:
		status = SpbUpdateSynchronously(SpbContext,
			REG_CURRENT_CTRL, REG_CURRENT_CTRL_FLASH_MASK,
			wData << REG_CURRENT_CTRL_FLASH_SHIFT);
		if (!NT_SUCCESS(status))
			goto end;
		break;

	case  IOCTL_FLED_BRT_TORCH:
		status = SpbUpdateSynchronously(SpbContext,
			REG_CURRENT_CTRL, REG_CURRENT_CTRL_TORCH_MASK,
			wData << REG_CURRENT_CTRL_TORCH_SHIFT);
		if (!NT_SUCCESS(status))
			goto end;
		break;
		
	case  IOCTL_FLED_FLASH_TOUT:
		status = SpbUpdateSynchronously(SpbContext,
			REG_FLASH_FEATURE, REG_FLASH_FEATURE_TIMEOUT_MASK,
			wData << REG_FLASH_FEATURE_TIMEOUT_SHIFT);
		if (!NT_SUCCESS(status))
			goto end;
		break;

	default:
		status = STATUS_NOT_SUPPORTED;
		break;
	}
end:
	if (!requestPending)
	{
		WdfRequestCompleteWithInformation(Request, status, sizeof(ULONG));
	}

	return;
}