/*++
Copyright (c) Texas Instruments Inc. All Rights Reserved.
Sample code for LED FLASH LM3642.

The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Module Name:

driver.c

Abstract:

This file contains the driver entry points and callbacks.

Environment:

Kernel-mode Driver Framework

--*/

#include "driver.h"
#include "driver.tmh"
#include "Public.h"

#ifdef ALLOC_PRAGMA
#pragma alloc_text (INIT, DriverEntry)
#pragma alloc_text (PAGE, TxnwLedLm3642EvtDeviceAdd)
#pragma alloc_text (PAGE, TxnwLedLm3642EvtDriverContextCleanup)
#endif

#define TXN_DEVICE_NAME L"TxnwLedLm3642"

NTSTATUS
DriverEntry(
	_In_ PDRIVER_OBJECT  DriverObject,
	_In_ PUNICODE_STRING RegistryPath
	)
	/*++

	Routine Description:
	DriverEntry initializes the driver and is the first routine called by the
	system after the driver is loaded. DriverEntry specifies the other entry
	points in the function driver, such as EvtDevice and DriverUnload.

	Parameters Description:

	DriverObject - represents the instance of the function driver that is loaded
	into memory. DriverEntry must initialize members of DriverObject before it
	returns to the caller. DriverObject is allocated by the system before the
	driver is loaded, and it is released by the system after the system unloads
	the function driver from memory.

	RegistryPath - represents the driver specific path in the Registry.
	The function driver can use the path to store driver related data between
	reboots. The path does not store hardware instance specific data.

	Return Value:

	STATUS_SUCCESS if successful,
	STATUS_UNSUCCESSFUL otherwise.

	--*/
{
	WDF_DRIVER_CONFIG config;
	NTSTATUS status;
	WDF_OBJECT_ATTRIBUTES attributes;

	//
	// Initialize WPP Tracing
	//
	WPP_INIT_TRACING(DriverObject, RegistryPath);


	//
	// Register a cleanup callback so that we can call WPP_CLEANUP when
	// the framework driver object is deleted during driver unload.
	//
	WDF_OBJECT_ATTRIBUTES_INIT(&attributes);
	attributes.EvtCleanupCallback = TxnwLedLm3642EvtDriverContextCleanup;

	WDF_DRIVER_CONFIG_INIT(&config,
		TxnwLedLm3642EvtDeviceAdd
		);

	status = WdfDriverCreate(DriverObject,
		RegistryPath,
		&attributes,
		&config,
		WDF_NO_HANDLE
		);

	if (!NT_SUCCESS(status)) {
		WPP_CLEANUP(DriverObject);
		return status;
	}

	return status;
}

NTSTATUS
TxnwLedLm3642EvtDeviceAdd(
	_In_    WDFDRIVER       Driver,
	_Inout_ PWDFDEVICE_INIT DeviceInit
	)
	/*++
	Routine Description:

	EvtDeviceAdd is called by the framework in response to AddDevice
	call from the PnP manager. We create and initialize a device object to
	represent a new instance of the device.

	Arguments:

	Driver - Handle to a framework driver object created in DriverEntry

	DeviceInit - Pointer to a framework-allocated WDFDEVICE_INIT structure.

	Return Value:

	NTSTATUS

	--*/
{
	WDF_OBJECT_ATTRIBUTES attributes;
	PDEVICE_EXTENSION devContext;
	WDFDEVICE fxDevice;
	WDF_PNPPOWER_EVENT_CALLBACKS pnpPowerCallbacks;
	WDF_IO_QUEUE_CONFIG queueConfig;
	NTSTATUS status;


	UNREFERENCED_PARAMETER(Driver);

	PAGED_CODE();

	WdfDeviceInitSetPowerPolicyOwnership(DeviceInit, FALSE);
	//
	// This driver handles D0 Entry and Exit to power on and off the
	// controller. It also handles prepare/release hardware so that it 
	// can acquire and free SPB resources needed to communicate with the  
	// touch controller.
	//
	WDF_PNPPOWER_EVENT_CALLBACKS_INIT(&pnpPowerCallbacks);

	pnpPowerCallbacks.EvtDeviceD0Entry = OnD0Entry;
	pnpPowerCallbacks.EvtDeviceD0Exit = OnD0Exit;
	pnpPowerCallbacks.EvtDevicePrepareHardware = OnPrepareHardware;
	pnpPowerCallbacks.EvtDeviceReleaseHardware = OnReleaseHardware;

	WdfDeviceInitSetPnpPowerEventCallbacks(DeviceInit, &pnpPowerCallbacks);

	//
	// Create a framework device object. This call will in turn create
	// a WDM device object, attach to the lower stack, and set the
	// appropriate flags and attributes.
	//
	WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE(&attributes, DEVICE_EXTENSION);

	status = WdfDeviceCreate(&DeviceInit, &attributes, &fxDevice);

	if (!NT_SUCCESS(status))
	{
		goto exit;
	}

	devContext = GetDeviceContext(fxDevice);
	devContext->FxDevice = fxDevice;

	//
	// Create a parallel dispatch queue to handle requests
	//
	WDF_IO_QUEUE_CONFIG_INIT_DEFAULT_QUEUE(
		&queueConfig,
		WdfIoQueueDispatchSequential);

	queueConfig.EvtIoDeviceControl = OnDeviceControl;
	queueConfig.PowerManaged = WdfFalse;

	status = WdfIoQueueCreate(
		fxDevice,
		&queueConfig,
		WDF_NO_OBJECT_ATTRIBUTES,
		&devContext->DefaultQueue);

	if (!NT_SUCCESS(status))
	{
		goto exit;
	}

	//
	// Register a manual I/O queue for Read Requests. This queue will be used 
	// for storing read-requests until data is available to complete them.
	//
	WDF_IO_QUEUE_CONFIG_INIT(&queueConfig, WdfIoQueueDispatchManual);

	queueConfig.PowerManaged = WdfFalse;

	status = WdfIoQueueCreate(
		fxDevice,
		&queueConfig,
		WDF_NO_OBJECT_ATTRIBUTES,
		&devContext->PingPongQueue);

	if (!NT_SUCCESS(status))
	{
		goto exit;
	}
	//
	// Register one last manual I/O queue for parking idle power
	// requests. This queue stores idle requests until they're cancelled,
	// and could be used to complete a wait/wake request.
	//

	WDF_IO_QUEUE_CONFIG_INIT(&queueConfig, WdfIoQueueDispatchManual);

	queueConfig.PowerManaged = WdfFalse;

	status = WdfIoQueueCreate(
		fxDevice,
		&queueConfig,
		WDF_NO_OBJECT_ATTRIBUTES,
		&devContext->IdleQueue
		);

	if (!NT_SUCCESS(status))
	{
		goto exit;
	}

	DECLARE_CONST_UNICODE_STRING(symbolicLink, L"\\DosDevices\\" LM3642_DEVICE_NAME);
	status = WdfDeviceCreateSymbolicLink(fxDevice, &symbolicLink);

exit:
	return status;
}

VOID
TxnwLedLm3642EvtDriverContextCleanup(
	_In_ WDFOBJECT DriverObject
	)
	/*++
	Routine Description:

	Free all the resources allocated in DriverEntry.

	Arguments:

	DriverObject - handle to a WDF Driver object.

	Return Value:

	VOID.

	--*/
{
	UNREFERENCED_PARAMETER(DriverObject);

	PAGED_CODE();

	WPP_CLEANUP(WdfDriverWdmGetDriverObject((WDFDRIVER)DriverObject));
}
