/*++
Copyright (c) Texas Instruments Inc. All Rights Reserved.
Sample code for Dual LED FLASH LM3643.

The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Module Name:

    device.h

Abstract:

    This file contains the device definitions.

Environment:

    Kernel-mode Driver Framework

--*/



#pragma once


EVT_WDF_DEVICE_D0_ENTRY OnD0Entry;

EVT_WDF_DEVICE_D0_EXIT OnD0Exit;

EVT_WDF_INTERRUPT_ISR OnInterruptIsr;

EVT_WDF_DEVICE_PREPARE_HARDWARE OnPrepareHardware;

EVT_WDF_DEVICE_RELEASE_HARDWARE OnReleaseHardware;

EVT_WDF_IO_QUEUE_IO_DEVICE_CONTROL OnDeviceControl;
