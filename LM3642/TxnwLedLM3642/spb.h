/*++
The Microsoft Public License (MS-PL)
Copyright (c) 2015 Microsoft

This license governs use of the accompanying software. If you use the software, you
accept this license. If you do not accept the license, do not use the software.

1. Definitions
The terms "reproduce," "reproduction," "derivative works," and "distribution" have
the same meaning here as under U.S. copyright law.
A "contribution" is the original software, or any additions or changes to the software.
A "contributor" is any person that distributes its contribution under this license.
"Licensed patents" are a contributor's patent claims that read directly on its contribution.

2. Grant of Rights
(A) Copyright Grant- Subject to the terms of this license, including the license conditions
and limitations in section 3, each contributor grants you a non-exclusive, worldwide,
royalty-free copyright license to reproduce its contribution, prepare derivative works of
its contribution, and distribute its contribution or any derivative works that you create.
(B) Patent Grant- Subject to the terms of this license, including the license conditions
and limitations in section 3, each contributor grants you a non-exclusive, worldwide,
royalty-free license under its licensed patents to make, have made, use, sell, offer for
sale, import, and/or otherwise dispose of its contribution in the software or derivative
works of the contribution in the software.

3. Conditions and Limitations
(A) No Trademark License- This license does not grant you rights to use any contributors'
name, logo, or trademarks.
(B) If you bring a patent claim against any contributor over patents that you claim are
infringed by the software, your patent license from such contributor to the software ends
automatically.
(C) If you distribute any portion of the software, you must retain all copyright, patent,
trademark, and attribution notices that are present in the software.
(D) If you distribute any portion of the software in source code form, you may do so only
under this license by including a complete copy of this license with your distribution.
If you distribute any portion of the software in compiled or object code form, you may
only do so under a license that complies with this license.
(E) The software is licensed "as-is." You bear the risk of using it. The contributors
give no express warranties, guarantees or conditions. You may have additional consumer
rights under your local laws which this license cannot change. To the extent permitted
under your local laws, the contributors exclude the implied warranties of merchantability,
fitness for a particular purpose and non-infringement.
Sample code. Dealpoint ID #843729.

Module Name:

spb.h

Abstract:

This module contains the touch driver I2C helper definitions.

Environment:

Kernel Mode

Revision History:
    Modified for LED driver. 

--*/

#pragma once

#include <wdm.h>
#include <wdf.h>

#define DEFAULT_SPB_BUFFER_SIZE 64

//
// SPB (I2C) context
//

typedef struct _SPB_CONTEXT
{
	WDFIOTARGET SpbIoTarget;
	LARGE_INTEGER I2cResHubId;
	WDFMEMORY WriteMemory;
	WDFMEMORY ReadMemory;
	WDFWAITLOCK SpbLock;
} SPB_CONTEXT;

NTSTATUS
SpbReadDataSynchronously(
	_In_ SPB_CONTEXT *SpbContext,
	_In_ UCHAR Address,
	_In_reads_bytes_(Length) PVOID Data,
	_In_ ULONG Length
	);

VOID
SpbTargetDeinitialize(
	IN WDFDEVICE FxDevice,
	IN SPB_CONTEXT *SpbContext
	);

NTSTATUS
SpbTargetInitialize(
	IN WDFDEVICE FxDevice,
	IN SPB_CONTEXT *SpbContext
	);

NTSTATUS
SpbWriteDataSynchronously(
	IN SPB_CONTEXT *SpbContext,
	IN UCHAR Address,
	IN PVOID Data,
	IN ULONG Length
	);

NTSTATUS
SpbUpdateSynchronously(
	IN SPB_CONTEXT *SpbContext,
	IN UCHAR Address,
	IN UCHAR Mask,
	IN UCHAR Data
	);