/*++
Copyright (c) Texas Instruments Inc. All Rights Reserved.
Sample code for LED FLASH LM3642.

The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Module Name:

driver.h

Abstract:

This file contains the driver definitions.

Environment:

Kernel-mode Driver Framework

--*/

#define INITGUID

#include <ntddk.h>
#include <wdf.h>

#include "device.h"
#include "spb.h"
#include "trace.h"

EXTERN_C_START

//
// WDFDRIVER Events
//

DRIVER_INITIALIZE DriverEntry;
EVT_WDF_DRIVER_DEVICE_ADD TxnwLedLm3642EvtDeviceAdd;
EVT_WDF_OBJECT_CONTEXT_CLEANUP TxnwLedLm3642EvtDriverContextCleanup;

EXTERN_C_END


#pragma once

#include <wdm.h>
#define RESHUB_USE_HELPER_ROUTINES
#include <reshub.h>

#define LED_POOL_TAG                  (ULONG)'cuoT'

typedef struct _DEVICE_EXTENSION
{
	WDFDEVICE FxDevice;
	WDFQUEUE DefaultQueue;
	WDFQUEUE PingPongQueue;
	WDFQUEUE IdleQueue;// Power related

	BOOLEAN ServiceInterruptsAfterD0Entry;
	SPB_CONTEXT I2CContext;

} DEVICE_EXTENSION, *PDEVICE_EXTENSION;

WDF_DECLARE_CONTEXT_TYPE_WITH_NAME(DEVICE_EXTENSION, GetDeviceContext)
