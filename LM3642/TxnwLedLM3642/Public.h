/*++
Copyright (c) Texas Instruments Inc. All Rights Reserved.
Sample code for LED FLASH LM3642.

The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Module Name:

public.h

Abstract:

This module contains the common declarations shared by driver
and user applications.

Environment:

user and kernel

--*/


DEFINE_GUID(GUID_DEVINTERFACE_TxnwLedLm3642,
	0x1851bf6e, 0x6f89, 0x4e0e, 0x87, 0x74, 0x34, 0x1e, 0x44, 0x2e, 0x7c, 0xe9);

#define LM3642_DEVICE_NAME L"OEM_LED"
#define LM3642_DEVICE_PATH L"\\\\.\\" LM3642_DEVICE_NAME

#define FILE_DEVICE_FLED 0xffffUL

#define IOCTL_FLED_DEVID				CTL_CODE(FILE_DEVICE_FLED, 0x101, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_FLED_MODE					CTL_CODE(FILE_DEVICE_FLED, 0x102, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_FLED_TORCH_PIN_ENABLE		CTL_CODE(FILE_DEVICE_FLED, 0x103, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_FLED_STROBE_PIN_ENABLE	CTL_CODE(FILE_DEVICE_FLED, 0x104, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_FLED_STATUS				CTL_CODE(FILE_DEVICE_FLED, 0x105, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_FLED_ENABLE				CTL_CODE(FILE_DEVICE_FLED, 0x110, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_FLED_BRT_FLASH			CTL_CODE(FILE_DEVICE_FLED, 0x111, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_FLED_BRT_TORCH			CTL_CODE(FILE_DEVICE_FLED, 0x112, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_FLED_FLASH_TOUT			CTL_CODE(FILE_DEVICE_FLED, 0x150, METHOD_BUFFERED, FILE_ANY_ACCESS)

